package com.gobilliontest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class MoviesListResponse(
    var copyright: String? = null,
    var has_more: Boolean? = null,
    var num_results: Int? = null,
    var results: MutableList<MovieDetail>? = null,
    var status: String? = null
) {}

@Parcelize
data class MovieDetail(
    var byline: String? = null,
    var critics_pick: Int? = null,
    var date_updated: String? = null,
    var display_title: String? = null,
    var headline: String? = null,
    var link: Link? = null,
    var mpaa_rating: String? = null,
    var multimedia: Multimedia? = null,
    var opening_date: String? = null,
    var publication_date: String? = null,
    var summary_short: String? = null
) : Parcelable

@Parcelize
data class Link(
    var suggested_link_text: String? = null,
    var type: String? = null,
    var url: String? = null
):Parcelable


@Parcelize
data class Multimedia(
    var height: Int? = null,
    var src: String? = null,
    var type: String? = null,
    var width: Int? = null
):Parcelable

