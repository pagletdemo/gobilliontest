package com.gobilliontest.repository

import com.gobilliontest.network.RetrofitService

class MoviesRepository(private val retrofitService: RetrofitService) {

    suspend fun getAllMovies(apiKey: String) = retrofitService.getAllMovies(apiKey)

}