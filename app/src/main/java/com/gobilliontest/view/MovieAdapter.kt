package com.gobilliontest.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gobilliontest.R
import com.gobilliontest.databinding.InflateMovieItemBinding
import com.gobilliontest.model.MovieDetail
import kotlinx.android.synthetic.main.inflate_movie_item.view.*


class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private lateinit var movieListener: OnMovieClickListener

    private val differCallback = object : DiffUtil.ItemCallback<MovieDetail>() {
        override fun areItemsTheSame(oldItem: MovieDetail, newItem: MovieDetail): Boolean {
            return oldItem.display_title == newItem.display_title
        }

        override fun areContentsTheSame(oldItem: MovieDetail, newItem: MovieDetail): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val binding: InflateMovieItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.inflate_movie_item, parent, false
        )

        return MovieViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun setClickListener(repoListener: OnMovieClickListener) {
        this.movieListener = repoListener
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val itemInfo = differ.currentList[position]
        holder.bind(itemInfo)

        Glide.with(holder.itemView)
            .load(itemInfo.multimedia?.src)
            .centerCrop()
            .placeholder(R.mipmap.ic_launcher_round)
            .into(holder.itemView.ivMovie)


        holder.itemView.setOnClickListener {
            movieListener.onMovieClick(itemInfo)
        }

    }

    class MovieViewHolder(private var binding: InflateMovieItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MovieDetail) {
            binding.data = item
            binding.executePendingBindings()
        }
    }

}

interface OnMovieClickListener {
    fun onMovieClick(movieDetail: MovieDetail)
}

