package com.gobilliontest.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.gobilliontest.R
import com.gobilliontest.common.Constants
import com.gobilliontest.common.Util
import com.gobilliontest.model.MovieDetail
import com.gobilliontest.network.RetrofitService
import com.gobilliontest.repository.MoviesRepository
import com.gobilliontest.viewModel.MovieViewModelFactory
import com.gobilliontest.viewModel.MoviesListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnMovieClickListener {

    private lateinit var viewModel: MoviesListViewModel
    private val adapter = MovieAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvMovies.adapter = adapter
        adapter.setClickListener(this)

        initializeViewModel()
        observeData()

        if (Util.checkNetwork(this)) {
            viewModel.getAllMovies(getString(R.string.api_key))
        } else {
            Toast.makeText(this, getString(R.string.check_internet), Toast.LENGTH_SHORT).show()
        }

    }

    private fun initializeViewModel() {

        val retrofitService = RetrofitService.getInstance()
        val mainRepository = MoviesRepository(retrofitService)

        viewModel = ViewModelProvider(
            this,
            MovieViewModelFactory(mainRepository)
        ).get(MoviesListViewModel::class.java)

    }

    private fun observeData() {
        viewModel.movieList.observe(this, {
            adapter.differ.submitList(it)
        })

        viewModel.errorMessage.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })

        viewModel.loading.observe(this, {
            progressBar.isVisible = it
        })

    }

    override fun onMovieClick(movieDetail: MovieDetail) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(Constants.MOVIE_DATA, movieDetail)
        startActivity(intent)
    }

}